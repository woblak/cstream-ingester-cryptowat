package com.woblak.cstream.ingester.cryptowat.api.constant;

public abstract class CryptowatHeaders {

    public static final String CRYPTOWAT_PUBLIC_KEY = "X-CW-API-Key";
}
