package com.woblak.cstream.ingester.cryptowat.api.constant;

public abstract class CryptocompareAction {

    public static final String SUBSCRIBE = "SubAdd";
    public static final String UNSUBSCRIBE = "SubRemove";
}
