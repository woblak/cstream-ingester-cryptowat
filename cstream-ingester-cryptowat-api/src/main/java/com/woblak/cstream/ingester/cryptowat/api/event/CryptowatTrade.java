package com.woblak.cstream.ingester.cryptowat.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CryptowatTrade {

    private String code;
    private String provider;
    private int replicaNumber;

    private Market market;
    private Trade trade;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Market {
        String exchangeId;
        String currencyPairId;
        String marketId;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Trade {
        String externalId;
        Long timestampNano;
        BigDecimal price;
        BigDecimal amount;
        BigDecimal amountQuote;
        String orderSide;
    }
}
