package com.woblak.cstream.ingester.cryptowat.api.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum OrderSide {

    BUY("buy"),
    SELL("sel"),
    UNKNOWN("unknown"),
    EMPTY("empty");

    private final String value;
}
