package com.woblak.cstream.ingester.cryptowat.api.message;

import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RequiredArgsConstructor
@Getter
@Builder
public class CryptowatTradeMsg implements Message<CryptowatTrade> {

    public static final String EVENT = "crypto-trade-data";
    public static final String VERSION = "1.0";

    private final MessageHeaders headers;
    private final CryptowatTrade payload;
}
