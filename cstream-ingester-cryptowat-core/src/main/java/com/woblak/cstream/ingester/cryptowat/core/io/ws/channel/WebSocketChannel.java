package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel;

import org.springframework.messaging.MessageChannel;

import java.io.IOException;
import java.nio.channels.Channel;
import java.util.concurrent.ExecutionException;

public interface WebSocketChannel extends MessageChannel, Channel {

    String getKey();

    String getUrl();

    void open() throws IOException, ExecutionException, InterruptedException;
}
