package com.woblak.cstream.ingester.cryptowat.core.io.ws.mapper;

import ch.cryptowat.protocol.TradeRecord;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woblak.cstream.ingester.cryptowat.api.constant.OrderSide;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.CryptowatWsChannelParams;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CryptowatTradeRecordsMapper {

    private final ObjectMapper om;

    public TradeRecord deserializeToTradeRecords(String json) throws JsonProcessingException {
        return om.readValue(json, TradeRecord.class);
    }

    public List<CryptowatTrade> mapToCryptowatTradeRecords(TradeRecord record, CryptowatWsChannelParams params) {
        if (record == null) {
            return Collections.emptyList();
        }
        CryptowatTrade.Market market = mapToCryptowatTradeRecordMarket(record.getMarketUpdate().getMarket());
        return record.getMarketUpdate().getTradesUpdate().getTrades().stream()
                .map(this::mapToCryptowatTradeRecordTrade)
                .map(i -> mapToCryptowatTradedRecord(i, market, params))
                .collect(Collectors.toList());
    }

    private CryptowatTrade.Market mapToCryptowatTradeRecordMarket(TradeRecord.MarketUpdate.Market market) {
        if (market == null) {
            return null;
        }
        return CryptowatTrade.Market.builder()
                .exchangeId(market.getExchangeId())
                .marketId(market.getMarketId())
                .currencyPairId(market.getCurrencyPairId())
                .build();
    }

    private CryptowatTrade mapToCryptowatTradedRecord(CryptowatTrade.Trade trade,
                                                      CryptowatTrade.Market market,
                                                      CryptowatWsChannelParams params) {
        return CryptowatTrade.builder()
                .code(params.getCode())
                .provider(params.getKey())
                .replicaNumber(params.getReplicaNumber())
                .market(market)
                .trade(trade)
                .build();
    }

    private CryptowatTrade.Trade mapToCryptowatTradeRecordTrade(TradeRecord.MarketUpdate.TradesUpdate.Trade trade) {
        if (trade == null) {
            return null;
        }
        return CryptowatTrade.Trade.builder()
                .externalId(trade.getExternalId())
                .timestampNano(toLong(trade.getTimestampNano()))
                .price(toBigDecimal(trade.getPriceStr()))
                .amount(toBigDecimal(trade.getAmountStr()))
                .amountQuote(toBigDecimal(trade.getAmountQuoteStr()))
                .orderSide(Optional.ofNullable(trade.getOrderSide()).orElseGet(OrderSide.EMPTY::getValue))
                .build();
    }

    private Long toLong(String str) {
        return isBlank(str) ? null : Long.valueOf(str);
    }

    private BigDecimal toBigDecimal(String str) {
        return isBlank(str) ? null : new BigDecimal(str);
    }

    private boolean isBlank(String str) {
        return str == null || str.isEmpty();
    }
}
