package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "websocket.channel.cryptowat")
@Data
public class CryptowatWsChannelParams {

    private String key;
    private String url;
    private String userPublicKey;
    private int minMillisToReconnect;
    private List<String> resources;
    private String code;
    private int replicaNumber;
}
