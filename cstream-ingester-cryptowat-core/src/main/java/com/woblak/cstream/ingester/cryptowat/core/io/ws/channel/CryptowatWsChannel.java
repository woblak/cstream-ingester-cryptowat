package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel;

import com.woblak.cstream.ingester.cryptowat.api.constant.CryptowatHeaders;
import com.woblak.cstream.ingester.cryptowat.core.io.kafka.producer.KafkaSender;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.support.Subscriber;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.support.WsConnector;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.handler.WsMsgHandler;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.listener.CryptowatInstrumentsTradesListener;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.mapper.SubscriptionMapper;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.mapper.CryptowatTradeRecordsMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;

import java.util.Set;

@Service
@Scope("singleton")
@Slf4j
public class CryptowatWsChannel extends AbstractWsChannel {

    private final CryptowatInstrumentsTradesListener cryptowatInstrumentsTradesHandler;
    private final SubscriptionMapper subscriptionMapper;
    private final CryptowatWsChannelParams params;

    @Autowired
    public CryptowatWsChannel(
            WebSocketClient webSocketClient,
            WsConnector wsConnector,
            Subscriber subscriber,
            CryptowatTradeRecordsMapper cryptowatTradeRecordsMapper,
            SubscriptionMapper subscriptionMapper,
            KafkaSender kafkaSender,
            Set<WsMsgHandler> wsHandlers,
            CryptowatWsChannelParams params
    ) {
        super(webSocketClient, wsConnector, subscriber);
        this.cryptowatInstrumentsTradesHandler = new CryptowatInstrumentsTradesListener(
                params.getMinMillisToReconnect(), this, cryptowatTradeRecordsMapper, kafkaSender, wsHandlers);
        this.subscriptionMapper = subscriptionMapper;
        this.params = params;
    }

    @Override
    public String getKey() {
        return params.getKey();
    }

    @Override
    public String getUrl() {
        return params.getUrl();
    }

    @Override
    protected WebSocketHandler getListener() {
        return cryptowatInstrumentsTradesHandler;
    }

    @Override
    protected WebSocketHttpHeaders getWebSocketHttpHeaders() {
        var httpHeaders = new HttpHeaders();
        httpHeaders.add(CryptowatHeaders.CRYPTOWAT_PUBLIC_KEY, params.getUserPublicKey());
        return new WebSocketHttpHeaders(httpHeaders);
    }

    /**
     * Example of payload to send to subscribe:
     * {"subscribe":{"subscriptions":[{"streamSubscription":{"resource":"markets:*:trades"}}]}}
     */
    @Override
    protected Object getSubscriptionRequest() {
        return subscriptionMapper.mapToSubscribeRequest(params.getResources());
    }

    @Override
    protected Object getUnsubscriptionRequest() {
        return subscriptionMapper.mapToUnsubscribeRequest(params.getResources());
    }
}
