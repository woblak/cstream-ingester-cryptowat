package com.woblak.cstream.ingester.cryptowat.core.io.ws.sender;

import org.springframework.messaging.Message;

import java.util.concurrent.Future;

public interface WsSender {

    Future<SendResult> send(Message<?> msg);
}
