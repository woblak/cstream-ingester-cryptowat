package com.woblak.cstream.ingester.cryptowat.core.exception;

public class WebSocketSubscriptionException extends RuntimeException {

    public WebSocketSubscriptionException(String message) {
        super(message);
    }

    public WebSocketSubscriptionException(String message, Throwable cause) {
        super(message, cause);
    }
}
