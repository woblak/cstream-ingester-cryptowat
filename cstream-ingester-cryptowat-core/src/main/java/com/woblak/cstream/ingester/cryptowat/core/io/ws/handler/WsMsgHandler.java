package com.woblak.cstream.ingester.cryptowat.core.io.ws.handler;

import org.springframework.web.socket.WebSocketSession;

public interface WsMsgHandler {

    boolean test(String content);

    void apply(WebSocketSession session, String content);
}
