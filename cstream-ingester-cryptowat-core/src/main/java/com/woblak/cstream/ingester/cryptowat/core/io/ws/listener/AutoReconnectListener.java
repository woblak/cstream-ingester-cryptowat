package com.woblak.cstream.ingester.cryptowat.core.io.ws.listener;

import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.AbstractWsChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class AutoReconnectListener extends AbstractWsListener {

    private final int minMillisToReconnect;
    private final AbstractWsChannel channel;

    public AutoReconnectListener(int minMillisToReconnect, AbstractWsChannel channel) {
        this.minMillisToReconnect = minMillisToReconnect;
        this.channel = channel;
    }

    private int attemtsToReconnect = 0;

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        super.afterConnectionEstablished(webSocketSession);
        attemtsToReconnect = 0;
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        super.afterConnectionClosed(session, closeStatus);
        try {
            reconnect();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            reconnect();
        }
    }

    private void reconnect() throws InterruptedException, IOException, ExecutionException {
        attemtsToReconnect++;
        int reconnectAfterMillis = minMillisToReconnect * attemtsToReconnect * attemtsToReconnect;
        log.info("Reconect in [{}] millis", reconnectAfterMillis);
        TimeUnit.MILLISECONDS.sleep(reconnectAfterMillis);
        if (!channel.isOpen()) {
            channel.open();
        }
    }
}
