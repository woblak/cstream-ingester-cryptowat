package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.support;

import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ExecutionException;

public interface WsConnector {

    WebSocketSession connect(
            String url,
            WebSocketHttpHeaders webSocketHttpHeaders,
            WebSocketHandler webSocketHandler
    ) throws ExecutionException, InterruptedException;
}
