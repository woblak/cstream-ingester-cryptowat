package com.woblak.cstream.ingester.cryptowat.core.config;

import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.CryptowatWsChannel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class OnExit {

    private final CryptowatWsChannel cryptowatWsChannel;

    public void enableGracefulExit() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                cryptowatWsChannel.close();
                log.info("Closed gracefully");
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }));
    }
}
