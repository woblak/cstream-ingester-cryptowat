package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel;

import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.support.Subscriber;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.support.WsConnector;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public abstract class AbstractWsChannel implements WebSocketChannel {

    private final WebSocketClient webSocketClient;
    private final WsConnector wsConnector;
    private final Subscriber subscriber;

    private WebSocketSession session;

    @Override
    public synchronized void open() throws IOException, ExecutionException, InterruptedException {
        log.info("Opening connection to [{}] with headers [{}]", getUrl(), getWebSocketHttpHeaders());
        this.session = wsConnector.connect(getUrl(), getWebSocketHttpHeaders(), getListener());
        subscriber.subscribe(session, getSubscriptionRequest());
    }

    @Override
    public synchronized boolean isOpen() {
        return session != null && session.isOpen();
    }

    @Override
    public synchronized void close() throws IOException {
        subscriber.unsubscribe(session, getUnsubscriptionRequest());
        if (isOpen()) {
            session.close();
        }
    }

    @Override
    public synchronized boolean send(Message<?> message, long l) {
        var msg = new TextMessage(message.getPayload().toString().getBytes());
        try {
            session.sendMessage(msg);
        } catch (IOException e) {
            log.error("Failed sending to [{}] message [{}]", session.getUri(), msg);
            log.error(e.getMessage(), e);
        }
        return true;
    }

    protected abstract WebSocketHandler getListener();

    protected abstract WebSocketHttpHeaders getWebSocketHttpHeaders();

    protected abstract Object getSubscriptionRequest();

    protected abstract Object getUnsubscriptionRequest();
}
