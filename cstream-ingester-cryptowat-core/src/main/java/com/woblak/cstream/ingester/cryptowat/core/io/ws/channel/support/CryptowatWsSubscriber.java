package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.support;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woblak.cstream.ingester.cryptowat.core.exception.WebSocketSubscriptionException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.UUID;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CryptowatWsSubscriber implements Subscriber {

    private final ObjectMapper om;

    @Override
    public String subscribe(WebSocketSession session, Object request) throws IOException {
        String json = serialize(request);
        session.sendMessage(new TextMessage(json.getBytes()));
        return UUID.randomUUID().toString();
    }

    @Override
    public void unsubscribe(WebSocketSession session, Object request) throws IOException {
        String json = serialize(request);
        if (session != null && session.isOpen()) {
            session.sendMessage(new TextMessage(json.getBytes()));
        }
    }

    private String serialize(Object request) {
        String result = null;
        try {
            result = om.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new WebSocketSubscriptionException(e.getMessage(), e);
        }
        return result;
    }
}
