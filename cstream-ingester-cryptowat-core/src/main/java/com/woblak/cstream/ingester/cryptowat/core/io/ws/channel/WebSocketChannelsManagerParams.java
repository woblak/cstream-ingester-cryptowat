package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "websocket.manager")
@Data
public class WebSocketChannelsManagerParams {

    private List<String> channelsKeysEnabled;
}
