package com.woblak.cstream.ingester.cryptowat.core.exception;

public class WebSocketMessageHandlerException extends RuntimeException {

    public WebSocketMessageHandlerException(String message, Throwable cause) {
    super(message, cause);
}

    public WebSocketMessageHandlerException(String message) {
        super(message);
    }
}
