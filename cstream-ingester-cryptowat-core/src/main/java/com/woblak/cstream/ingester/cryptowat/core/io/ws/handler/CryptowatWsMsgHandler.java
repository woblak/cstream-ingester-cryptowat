package com.woblak.cstream.ingester.cryptowat.core.io.ws.handler;


import ch.cryptowat.protocol.TradeRecord;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import com.woblak.cstream.ingester.cryptowat.core.io.kafka.producer.CryptowatTradeRecordSender;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.CryptowatWsChannelParams;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.mapper.CryptowatTradeRecordsMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.time.Clock;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CryptowatWsMsgHandler implements WsMsgHandler {

    private static final String PREFIX = "marketUpdate";

    private final Clock clock;
    private final CryptowatWsChannelParams params;
    private final CryptowatTradeRecordsMapper cryptowatTradeRecordsMapper;
    private final CryptowatTradeRecordSender cryptowatTradeRecordSender;

    @Override
    public boolean test(String content) {
        return PREFIX.equals(content.substring(2, 14));
    }

    /**
     * Examples:
     * <p>
     * [{"marketUpdate":{
     * "market":{"exchangeId":"2","currencyPairId":"9","marketId":"65"},
     * "tradesUpdate":{
     * "trades":[{
     * "externalId":"167397236","timestamp":"1620653917","timestampNano":"1620653917834856000",
     * "priceStr":"57750.01","amountStr":"0.00084328","amountQuoteStr":"48.6994284328","orderSide":"BUYSIDE"
     * },{
     * "externalId":"167397237","timestamp":"1620653917","timestampNano":"1620653917834856000",
     * "priceStr":"57750.01","amountStr":"0.01613038","amountQuoteStr":"931.5296063038","orderSide":"BUYSIDE"
     * }]
     * }
     * }}]
     * <p>
     * [{"marketUpdate":{
     * "market":{"exchangeId":"1","currencyPairId":"9","marketId":"1"},
     * "tradesUpdate":{
     * "trades":[{
     * "externalId":"698248453","timestamp":"1620653918","timestampNano":"1620653918016000000",
     * "priceStr":"57762","amountStr":"0.2596","amountQuoteStr":"14995.0152","orderSide":"SELLSIDE"
     * }]
     * }
     * }}]
     */
    @Override
    public void apply(WebSocketSession session, String content) {
        try {
            long startTimestamp = clock.millis();
            TradeRecord tradeRecord = cryptowatTradeRecordsMapper.deserializeToTradeRecords(content);
            List<CryptowatTrade> cryptowatTrades =
                    cryptowatTradeRecordsMapper.mapToCryptowatTradeRecords(tradeRecord, params);
            cryptowatTrades.forEach(trade -> cryptowatTradeRecordSender.sendCryptowatTrade(trade, startTimestamp));

        } catch (Exception e) {
            log.error("cannot handle [{}] as TradeRecords", content);
        }
    }
}
