package com.woblak.cstream.ingester.cryptowat.core;

import com.woblak.cstream.ingester.cryptowat.core.config.OnExit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class AppMain {

    @Autowired
    private OnExit onExit;

    public static void main(String... args) {
        SpringApplication.run(AppMain.class, args);
    }

    @Bean
    public CommandLineRunner setUp() {
        return args -> onExit.enableGracefulExit();
    }

}
