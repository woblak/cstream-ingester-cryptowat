package com.woblak.cstream.ingester.cryptowat.core.io.ws.mapper;

import ch.cryptowat.protocol.SubscribeRequest;
import ch.cryptowat.protocol.UnsubscribeRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SubscriptionMapper {

    public SubscribeRequest mapToSubscribeRequest(List<String> resources) {
        List<SubscribeRequest.Subscribe.Subscription> subs = resources.stream()
                .map(SubscribeRequest.Subscribe.Subscription.StreamSubscription::new)
                .map(SubscribeRequest.Subscribe.Subscription::new)
                .collect(Collectors.toList());
        return new SubscribeRequest(new SubscribeRequest.Subscribe(subs));
    }

    public UnsubscribeRequest mapToUnsubscribeRequest(List<String> resources) {
        List<UnsubscribeRequest.Unsubscribe.Subscription> subs = resources.stream()
                .map(UnsubscribeRequest.Unsubscribe.Subscription.StreamSubscription::new)
                .map(UnsubscribeRequest.Unsubscribe.Subscription::new)
                .collect(Collectors.toList());
        return new UnsubscribeRequest(new UnsubscribeRequest.Unsubscribe(subs));
    }
}
