package com.woblak.cstream.ingester.cryptowat.core.exception;

public class WebSocketListenerException extends RuntimeException {

    public WebSocketListenerException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebSocketListenerException(String message) {
        super(message);
    }
}
