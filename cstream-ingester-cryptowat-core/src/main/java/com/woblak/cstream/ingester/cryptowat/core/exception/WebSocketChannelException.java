package com.woblak.cstream.ingester.cryptowat.core.exception;

public class WebSocketChannelException extends RuntimeException {

    public WebSocketChannelException(String message) {
        super(message);
    }

    public WebSocketChannelException(String message, Throwable cause) {
        super(message, cause);
    }
}
