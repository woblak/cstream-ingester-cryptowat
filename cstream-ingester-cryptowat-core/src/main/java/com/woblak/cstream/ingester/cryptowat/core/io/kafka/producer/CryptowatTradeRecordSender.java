package com.woblak.cstream.ingester.cryptowat.core.io.kafka.producer;

import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.ingester.cryptowat.api.event.CryptowatTrade;
import com.woblak.cstream.ingester.cryptowat.api.message.CryptowatTradeMsg;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CryptowatTradeRecordSender {

    private final KafkaSender kafkaSender;

    public void sendCryptowatTrade(CryptowatTrade cryptowatTrade, long lastTimestamp) {

        Message<CryptowatTrade> tradeRecordMessage = MessageBuilder.withPayload(cryptowatTrade)
                .setHeader(KafkaHeaders.TOPIC, KafkaTopics.CRYPTOWAT_TRADES)
                .setHeader(KafkaHeaders.EVENT, CryptowatTradeMsg.EVENT)
                .setHeader(KafkaHeaders.VERSION, CryptowatTradeMsg.VERSION)
                .setHeader(KafkaHeaders.MESSAGE_KEY, cryptowatTrade.getCode())
                .setHeader(KafkaHeaders.LAST_TIMESTAMP, lastTimestamp)
                .build();

        kafkaSender.send(tradeRecordMessage);
    }
}
