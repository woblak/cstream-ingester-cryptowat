package com.woblak.cstream.ingester.cryptowat.core.io.ws.protocol.cryptowat;

import ch.cryptowat.protocol.UnsubscribeRequest;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.protocol.SubscriptionRequest;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Getter
public class CryptowatUnsubReq implements SubscriptionRequest {

    private final UnsubscribeRequest unsubscribeRequest;
}
