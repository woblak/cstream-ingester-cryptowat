package com.woblak.cstream.ingester.cryptowat.core.io.ws.listener;

import com.woblak.cstream.ingester.cryptowat.core.io.kafka.producer.KafkaSender;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.AbstractWsChannel;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.handler.WsMsgHandler;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.mapper.CryptowatTradeRecordsMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.WebSocketSession;

import java.util.Set;

@Slf4j
public class CryptowatInstrumentsTradesListener extends AutoReconnectListener {

    private final CryptowatTradeRecordsMapper cryptowatTradeRecordsMapper;
    private final KafkaSender kafkaSender;
    private final Set<WsMsgHandler> wsHandlers;

    public CryptowatInstrumentsTradesListener(int minMillisToReconnect,
                                              AbstractWsChannel channel,
                                              CryptowatTradeRecordsMapper cryptowatTradeRecordsMapper,
                                              KafkaSender kafkaSender,
                                              Set<WsMsgHandler> wsHandlers) {
        super(minMillisToReconnect, channel);
        this.cryptowatTradeRecordsMapper = cryptowatTradeRecordsMapper;
        this.kafkaSender = kafkaSender;
        this.wsHandlers = wsHandlers;
    }

    @Override
    public void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
        String json = super.toString(message);
        log.info("<< ws << [{}] : [{}]", super.getConnectionDisplayName(session), json);
        wsHandlers.stream()
                .filter(handler -> handler.test(json))
                .forEach(handler -> handler.apply(session, json));
    }
}
