package com.woblak.cstream.ingester.cryptowat.core.io.ws.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Slf4j
public abstract class AbstractWsListener extends AbstractWebSocketHandler {

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        log.info("Opened connection to [{}]", getConnectionDisplayName(webSocketSession));
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        log.debug("<< ws << [{}] : [{}]", getConnectionDisplayName(session), message);
        super.handleMessage(session, message);
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String content = new String(message.getPayload().getBytes());
        log.info("<< ws << [{}] : [{}]", getConnectionDisplayName(session), content);
    }

    @Override
    public void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
        String content = toString(message);
        log.info("<< ws << [{}] : [{}]", getConnectionDisplayName(session), content);
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable throwable) throws Exception {
        log.error("Error on [{}]", getConnectionDisplayName(session));
        log.error(throwable.getMessage(), throwable);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        log.info("Closed connection to [{}]", getConnectionDisplayName(session));
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }


    protected String toString(BinaryMessage binaryMessage) {
        return StandardCharsets.UTF_8.decode(binaryMessage.getPayload()).toString();
    }

    protected String getConnectionDisplayName(WebSocketSession webSocketSession) {
        return Objects.requireNonNullElse(webSocketSession.getUri(), webSocketSession.getRemoteAddress()).toString();
    }
}
