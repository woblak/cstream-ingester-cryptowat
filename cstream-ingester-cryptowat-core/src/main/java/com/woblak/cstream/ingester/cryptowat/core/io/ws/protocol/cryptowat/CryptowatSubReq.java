package com.woblak.cstream.ingester.cryptowat.core.io.ws.protocol.cryptowat;


import ch.cryptowat.protocol.SubscribeRequest;
import com.woblak.cstream.ingester.cryptowat.core.io.ws.protocol.SubscriptionRequest;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Getter
public class CryptowatSubReq implements SubscriptionRequest {

    private final SubscribeRequest subscribeRequest;
}
