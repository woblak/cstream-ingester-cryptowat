package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.support;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;

import java.net.URI;
import java.util.concurrent.ExecutionException;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CryptowatWsConnector implements WsConnector {

    private final WebSocketClient webSocketClient;

    @Override
    public WebSocketSession connect(
            String url,
            WebSocketHttpHeaders webSocketHttpHeaders,
            WebSocketHandler webSocketHandler
    ) throws ExecutionException, InterruptedException {

        return webSocketClient.doHandshake(webSocketHandler, webSocketHttpHeaders, URI.create(url)).get();
    }
}
