package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel.support;

import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

public interface Subscriber {

    String subscribe(WebSocketSession webSocketSession, Object request) throws IOException;

    void unsubscribe(WebSocketSession webSocketSession, Object request) throws IOException;
}
