package com.woblak.cstream.ingester.cryptowat.core.config;

import com.woblak.cstream.ingester.cryptowat.core.io.kafka.producer.KafkaSender;
import kafka.server.KafkaConfigTest;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.header.Headers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.support.DefaultKafkaHeaderMapper;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.messaging.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@EmbeddedKafka
@Import({KafkaConfigTest.class})
public class EmbeddedKafkaTest {

    @Value("${test.kafka.topic}")
    protected String topic;

    @Autowired
    EmbeddedKafkaBroker embeddedKafkaBroker;

    @Autowired
    @Qualifier("test-consumerFactory")
    ConsumerFactory<String, Object> consumerFactory;

    @Autowired
    @Qualifier("test-containerProperties")
    ContainerProperties containerProperties;

    @Autowired
    KafkaSender kafkaMessageSender;

    @Autowired
    DefaultKafkaHeaderMapper kafkaHeaderMapper;

    KafkaMessageListenerContainer<String, Object> container;

    List<MessageListener<String, Object>> listeners = new ArrayList<>();

    protected void start() {
        this.container = initContainer();
        container.start();
        ContainerTestUtils.waitForAssignment(container, embeddedKafkaBroker.getPartitionsPerTopic());
    }

    protected void stop() {
        if (isAlive()) {
            container.stop();
        }
    }

    protected void restart() {
        stop();
        start();
    }

    protected boolean isAlive() {
        return container != null && container.isRunning();
    }

    protected void addListener(MessageListener<String, Object> listener) {
        listeners.add(listener);
    }

    protected Message<?> buildMessage(Message<?> msg) {
        return msg;
    }

    protected void sendMessage(Message<?> msg) {
        kafkaMessageSender.send(msg);
    }

    protected void consumeMessage(Consumer<String, Object> consumer) {
        embeddedKafkaBroker.consumeFromAnEmbeddedTopic(consumer, topic);
    }

    protected void setDefaultTopic(String topic) {
        this.topic = topic;
    }

    protected Map<String, Object> toHeadersMap(Headers headers) {
        Map<String, Object> result = new HashMap<>();
        kafkaHeaderMapper.toHeaders(headers, result);
        return result;
    }

    private KafkaMessageListenerContainer<String, Object> initContainer() {
        return initContainer(consumerFactory, containerProperties, createListener());
    }

    private KafkaMessageListenerContainer<String, Object> initContainer(
            ConsumerFactory<String, Object> consumerFactory,
            ContainerProperties containerProperties,
            MessageListener<String, Object> listener
    ) {
        KafkaMessageListenerContainer<String, Object> container =
                new KafkaMessageListenerContainer<>(consumerFactory, containerProperties);
        container.setupMessageListener(listener);
        return container;
    }

    private MessageListener<String, Object> createListener() {
        return msg -> listeners.forEach(listener -> listener.onMessage(msg));
    }

}
