package com.woblak.cstream.ingester.cryptowat.core.io.ws.channel;

import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.ingester.cryptowat.api.message.CryptowatTradeMsg;
import com.woblak.cstream.ingester.cryptowat.core.config.EmbeddedKafkaTest;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(properties = {
        "test.kafka.topic=cryptowat-trades"
})
class CryptowatWsChannelTest extends EmbeddedKafkaTest {

    @Autowired
    CryptowatWsChannel cryptowatWsChannel;

    BlockingQueue<ConsumerRecord<String, Object>> records;

    @BeforeEach
    void setUp() throws InterruptedException, ExecutionException, IOException {
        if (!cryptowatWsChannel.isOpen()) {
            cryptowatWsChannel.open();
        }
        records = new ArrayBlockingQueue<>(256);
        super.addListener(records::add);
        super.start();
    }

    @AfterEach
    void tearDown() throws IOException {
        cryptowatWsChannel.close();
        super.stop();
    }

    @Test
    void shouldGetTradeRecordFromCryptowatAndPassItToKafka() {
        await()
                .with()
                .alias("Wait until any message will be received.")
                .given()
                .atMost(5000L, TimeUnit.MILLISECONDS)
                .pollInterval(50, TimeUnit.MILLISECONDS)
                .then()
                .until(() -> !records.isEmpty());

        ConsumerRecord<String, Object> record = records.peek();
        Map<String, Object> headers = super.toHeadersMap(record.headers());

        assertEquals(CryptowatTradeMsg.EVENT, headers.get(KafkaHeaders.EVENT).toString());
        assertEquals(CryptowatTradeMsg.VERSION, headers.get(KafkaHeaders.VERSION).toString());
        assertEquals(100L, Long.parseLong(headers.get(KafkaHeaders.LAST_TIMESTAMP).toString()));
        assertEquals(1, records.stream().map(ConsumerRecord::partition).distinct().count());
    }
}
