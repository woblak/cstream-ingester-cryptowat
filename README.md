# cstream-ingester-cryptowat  

## About
Provides crypto currencies trades data from websockets of https://cryptowat.ch to the project's Kafka.

Documentation: https://docs.cryptowat.ch/websocket-api/data-subscriptions

Market:  
https://api.cryptowat.ch/markets  
Exchange:  
https://api.cryptowat.ch/exchanges  
Asset:  
https://api.cryptowat.ch/assets  
Instrument:  
https://api.cryptowat.ch/pairs  

## Run  
Tests connects to real websocket of Cryptowat and you need to update the token placed in yml to pass tests.  
  
To run you need:  
- running Kafka on port given in yaml properties file  
- use specific profile for instance: `-Dspring.profiles.active=dev`